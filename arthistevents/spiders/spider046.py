# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem
import re
class Spider046Spider(scrapy.Spider):
    name = "spider046"
    allowed_domains = ["saims.wp.st-andrews.ac.uk"]
    start_urls = (
        'http://saims.wp.st-andrews.ac.uk/seminars-2/',
    )

    def parse(self, response):
        info = response.xpath('//*[@id="post-23"]/div/p[1]/text()[2]').extract().pop()
        venue = re.findall(r"the.*", info).pop()
        dates = response.xpath('//*[@id="post-23"]/div/table/tbody/tr/td[1]/text()').extract()
        titles = response.xpath('//*[@id="post-23"]/div/table/tbody/tr/td[2]/p[2]/text()').extract()
        descriptions = response.xpath('//*[@id="post-23"]/div/table/tbody/tr').extract()
        for title, description, date in zip(titles, descriptions, dates):
        	item = EventItem()
        	item['url'] = response.url
        	item['itemtype'] = 'event'
        	item['title'] = title
        	item['description'] = description
        	item['date'] = date
        	item['venue'] = venue
        	yield item
