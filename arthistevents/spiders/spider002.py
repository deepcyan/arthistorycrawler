# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider002Spider(CrawlSpider):
    name = "spider002"
    allowed_domains = ["maynoothuniversity.ie"]
    start_urls = (
        'https://www.maynoothuniversity.ie/centre-irish-cultural-heritage',
        'https://www.maynoothuniversity.ie/centre-irish-cultural-heritage/news',
        'https://www.maynoothuniversity.ie/centre-irish-cultural-heritage/events',
    )
    rules = [
        Rule(LinkExtractor(allow=('https://www.maynoothuniversity.ie/centre-irish-cultural-heritage/news/.*', )), callback='parse_news_item'),
        Rule(LinkExtractor(allow=('https://www.maynoothuniversity.ie/centre-irish-cultural-heritage/events/archive', )), callback='parse_events_archive'),
        Rule(LinkExtractor(allow=('https://www.maynoothuniversity.ie/centre-irish-cultural-heritage/events/.*', )), callback='parse_events_item'),
    ]

    def parse_news_item(self, response):
        sel = Selector(response)

        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = sel.css('#page-title::text').extract()[0].strip('\r\n ')
        item['description'] = sel.xpath('//*[starts-with(@id, "node-news-")]/div/div[2]').extract()[0].strip('\r\n ')
        item['url'] = response.url.split('?')[0]

        item['published_date'] = sel.xpath('//*[starts-with(@id, "node-news-")]/div/div[1]/div[3]/span/text()').extract()[0]

        yield item

    def parse_events_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.css('#page-title::text').extract()[0].strip('\r\n ')
        item['description'] = sel.css('[id^=node-event-] > div.field-name-body').extract()[0].strip('\r\n ')
        item['url'] = response.url.split('?')[0]

        item['date'] = ''.join(sel.xpath('//*[starts-with(@id, "node-event-")]/div[1]/div[2]/span//text()').extract()).strip('\r\n ')
        item['venue'] = sel.xpath('//*[starts-with(@id, "node-event-")]/div[1]/div[3]/text()').extract()[1].strip('\r\n ')

        yield item

    def parse_events_archive(self, response):
        sel = Selector(response)

        for event_url in sel.xpath('//*[@id="region-content"]/div/div[2]/div/div/div/div[2]/div/div/div/h3/a/@href'):
            url = response.urljoin(event_url.extract())
            event_request = scrapy.Request(url, callback=self.parse_events_item)
            yield event_request

