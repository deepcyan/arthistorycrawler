# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from arthistevents.items import GenericItem


class Spider003Spider(CrawlSpider):
    name = "spider003"
    allowed_domains = ["tcd.ie"]
    start_urls = (
        'http://www.tcd.ie/Medieval_Renaissance/news-events/',
        'http://www.tcd.ie/Medieval_Renaissance/calendar-events/'
    )

    rules = [
        Rule(LinkExtractor(allow=('http://www.tcd.ie/Medieval_Renaissance/news-events/', )), callback='parse_news_items'),
        #Rule(LinkExtractor(allow=('http://www.tcd.ie/Medieval_Renaissance/calendar-events/', )), callback='parse_calendar_items'),
    ]


    def parse_news_items(self, response):
        
        titles = response.xpath('//*[@id="main-content"]/p/text()[2]').extract()[1:5]
        descriptions = response.xpath('//*[@id="main-content"]/p').extract()[4:8]
        for title, description in zip(titles, descriptions):
            item = GenericItem()

            item['itemtype'] = 'generic'
            item['title'] = title
            item['description'] = description
            item['url'] = response.url
            yield item

    # def parse_calendar_items(self, response):
    #     sel = Selector(response)
    #     pass
