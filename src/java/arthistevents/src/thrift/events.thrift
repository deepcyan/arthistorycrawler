namespace java com.sayandeep.arthistevents.thrift

enum AHEErrorCode {
    /** Cause of error is unknown or not provided */
	UNKNOWN,

	/** Content not found */
    NOT_FOUND,

    /** Unexpected problem with the service */
    INTERNAL_ERROR
}

exception AHEError {
  1: required AHEErrorCode type,
  2: optional string message
} 

enum AttributeType {
	TITLE = 1,
	DESCRIPTION = 2,
	LONG_DESCRIPTION = 3,
	DATE = 4,
	DATE_PUBLISHED = 5,
	DATE_START = 6,
	DATE_END = 7,
	URL = 8,
	VENUE = 9
}

enum ItemType {
	UNKNOWN,
	NEWS,
	EVENT,
	CFP,
	LECTURE,
	SEMINAR,
	SCHOOL
}

struct Attribute {
	1: required AttributeType type,
	2: required string value
}

struct Item {
	1: optional map<string, string> json,
	2: optional list<Attribute> attributes,
	3: optional ItemType type = ItemType.UNKNOWN
}

struct Newsletter {
	1: required list<Item> items,
	2: optional string date  
}

service AHEService {
	list<Item> getAll() throws (1: AHEError e)
}
