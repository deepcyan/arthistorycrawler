DROP TABLE IF EXISTS Item;

CREATE TABLE IF NOT EXISTS Item(id VARCHAR(256), data BLOB) ENGINE=InnoDB;