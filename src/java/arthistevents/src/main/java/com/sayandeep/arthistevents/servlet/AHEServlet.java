package com.sayandeep.arthistevents.servlet;

import javax.servlet.annotation.WebServlet;

import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.server.TServlet;

import com.sayandeep.arthistevents.thrift.AHEService;

/**
 * Servlet implementation class AHEService
 */
@WebServlet("/AHEService/")
public class AHEServlet extends TServlet {
  private static final long serialVersionUID = -5216998252801179639L;

  public AHEServlet() {
    super(new AHEService.Processor(new AHEServiceHandler()), new TJSONProtocol.Factory());
  }
}
