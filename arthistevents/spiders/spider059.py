# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem
import re

class Spider059Spider(scrapy.Spider):
    name = "spider059"
    allowed_domains = ["history.ac.uk"]
    start_urls = (
        'http://www.history.ac.uk/events/seminars/113',
    )

    def parse(self, response):
    	descriptions = response.xpath('//*[@id="block-system-main"]/div/table/tbody/tr').extract()
        dates = response.xpath('//td[@class="date"]/text()').extract()
        titles = response.xpath('//*[@id="block-system-main"]/div/table/tbody/tr/td[2]/strong/text()').extract()
        # item = EventItem()
        # item['date'] = dates
        # item['title'] = titles
        # yield item
        for date, title, description in zip(dates, titles, descriptions):
            item = EventItem()
            item['url'] = response.url
            item['title'] = title
            item['itemtype'] = 'event'
            item['description'] = description
            item['date'] = date.strip()
            
            item['venue'] = "".join(re.findall(r"(?<=Venue:).*?(?=<)", description))
            yield item


