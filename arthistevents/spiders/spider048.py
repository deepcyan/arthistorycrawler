# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector
from arthistevents.items import NewsItem, EventItem
import re

class Spider048Spider(scrapy.Spider):
    name = "spider048"
    allowed_domains = ["gla.ac.uk/"]
    start_urls = (
       'http://www.gla.ac.uk/schools/critical/research/researchcentresandnetworks/mrsg/events/',
        )

    def parse(self, response):
        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/p[1]/em/text()').extract().pop()
        
        item['url'] = response.url
        item['date'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/h4[1]/strong/text()').extract().pop()
        item['venue'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/p[2]/text()[1]').extract().pop()
        item['description'] =item['title'] + ',' + item['date'] + ',' + item['venue']
        yield item   
        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/p[3]/em/text()').extract().pop()
        
        item['url'] = response.url
        item['date'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/h4[2]/strong/text()').extract().pop()
        item['venue'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/p[4]/text()').extract().pop()
        item['description'] =item['title'] + ',' + item['date'] + ',' + item['venue']
        yield item  
        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/p[5]/em/text()').extract().pop()
        
        item['url'] = response.url
        item['date'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/h4[3]/strong/text()').extract().pop()
        item['venue'] = response.xpath('//*[@id="mainpage_forbottom"]/div/div[1]/p[6]/text()[1]').extract().pop()
        item['description'] =item['title'] + ',' + item['date'] + ',' + item['venue']
        yield item      

            