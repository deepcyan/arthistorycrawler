# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from arthistevents.items import GenericItem


class Spider004Spider(CrawlSpider):
    name = "spider004"
    allowed_domains = ["tcd.ie"]
    start_urls = (
        'https://www.tcd.ie/History_of_Art/news/',
    )

    rules = [
        Rule(LinkExtractor(allow=('https://www.tcd.ie/History_of_Art/news/',)), callback='parse_news_items'),
    ]

    def parse_news_items(self, response):
        sel = Selector(response)
        for paragraph in sel.xpath('//*[@id="main-content"]/p'):
            item = GenericItem()
            item['itemtype'] = 'generic'
            item['description'] = paragraph.extract().strip('\r\n ')
            yield item
        pass
