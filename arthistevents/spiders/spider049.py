# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem
import re

class Spider049Spider(scrapy.Spider):
    name = "spider049"
    allowed_domains = ["www2.le.ac.uk"]
    start_urls = (
        'http://www2.le.ac.uk/departments/arthistory/events/',
    )

    def parse_news_item(self, response):
        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = response.xpath('//*[@id="parent-fieldname-title"]/text()').extract()[0].strip()
        item['description'] = response.xpath('//*[@id="content"]').extract()
        item['url'] = response.url
        item['published_date'] = " ".join(re.findall(r'(\w+)', response.xpath('//*[@id="content"]/div/div[1]/text()').extract()[1]))
        yield item

    def parse_news_list(self,response):
    	for event_url in response.xpath('//*[@id="content-core"]/dl/div/h2/a/@href').extract():
            event_request = scrapy.Request(event_url, callback=self.parse_news_item)
            yield event_request
        
    def parse(self, response):
    	urls = []
    	for i in range(0, 65, 5):
    		urls.append(response.urljoin('?b_start:int=' + str(i)))
    	for url in urls:
    		request = scrapy.Request(url, callback=self.parse_news_list)
    		yield request
        
    
