# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider068Spider(CrawlSpider):
    name = "spider068"
    allowed_domains = ["pims.ca"]
    start_urls = (
        'http://www.pims.ca/academics/calendar-of-events',
    )
    rules = [
        Rule(LinkExtractor(allow=('http://www.pims.ca/academics/calendar-of-events/event/.*', )), callback='parse_event_item'),
    ]

    

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="main"]/article/header/h1/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="main"]/article/div[2]').extract().pop()
        item['url'] = response.url

        item['date'] = sel.xpath('//*[@id="main"]/article/header/p[1]/time/text()').extract().pop()
        item['venue'] = sel.xpath('//*[@id="main"]/article/header/p[2]/text()').extract().pop()

        yield item

    
