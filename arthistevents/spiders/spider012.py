# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import GenericItem
class Spider012Spider(scrapy.Spider):
    name = "spider012"
    allowed_domains = ["ucd.ie"]
    start_urls = (
        'http://www.ucd.ie/mocleirigh/newsandevents/',
    )

    def parse(self, response):
    	titles = response.xpath('//*[@id="content"]/h2/text()').extract()[2:]
    	descriptions = response.xpath('//*[@id="content"]/p').extract()[9:-7]
    	descriptions[3] = descriptions[3] + descriptions.pop(4)+ descriptions.pop(5)+ descriptions.pop(6)
    	for title, description in zip(titles, descriptions):
    		item = GenericItem()
    		item['title'] = title
    		item['description'] = description
    		item['itemtype'] = 'generic'
    		item['url'] = response.url
    		yield item
