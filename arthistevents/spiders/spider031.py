# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem
import re

class Spider031Spider(scrapy.Spider):
    name = "spider031"
    allowed_domains = ["eca.ed.ac.uk"]
    start_urls = (
        'http://www.eca.ed.ac.uk/eca-home/news-events',
    )
    def parse(self, response):
    	dates = response.xpath('//*[@id="block-views-news-school-events"]/div/div/div[1]/div/div[1]/div/div/span/@content').extract()
        venues = response.xpath('//*[@id="block-views-news-school-events"]/div/div/div[1]/div/div[2]/span/div[2]/div/span/text()').extract()
        titles = response.xpath('//*[@id="block-views-news-school-events"]/div/div/div[1]/div/div[2]/span/a/strong/text()').extract()
        descriptions = response.xpath('//*[@id="block-views-news-school-events"]/div/div/div[1]/div').extract()
        for venue, date, title, description in zip(venues, dates, titles, descriptions):
            item = EventItem()
            item['url'] = response.url
            item['title'] = title
            item['venue'] = venue 
            item['itemtype'] = 'event'
            item['description'] = description
            item['date'] = date

            yield item
