# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem

class Spider060Spider(scrapy.Spider):
    name = "spider060"
    allowed_domains = ["history.ac.uk"]
    start_urls = (
        'http://www.history.ac.uk/events/seminars/127',
    )

    def parse(self, response):
        venue = response.xpath('//*[@id="block-system-main"]/div/div/p[2]/text()[2]').extract().pop()
        dates = response.xpath('//td[@class="date"]/text()').extract()
        titles = response.xpath('//*[@id="block-system-main"]/div/table/tbody/tr/td[2]/strong/text()').extract()
        descriptions = response.xpath('//*[@id="block-system-main"]/div/table/tbody/tr').extract()
        for date, title, description in zip(dates, titles, descriptions):
        	item = EventItem()
        	item['url'] = response.url
        	item['itemtype'] = 'event'
        	item['title'] = title
        	item['description'] = description
        	item['date'] = date.strip()
        	item['venue'] = venue
        	yield item

