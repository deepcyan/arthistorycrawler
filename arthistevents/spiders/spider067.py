# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from arthistevents.items import NewsItem

class Spider067Spider(CrawlSpider):
    name = "spider067"
    allowed_domains = ["apahau.org"]
    start_urls = (
        'http://blog.apahau.org',
    )

    def parse(self, response):
        
        item = NewsItem()
        item['url'] = response.url
        item['title'] = response.xpath('//a[@rel="bookmark"]/text()').extract().pop()
        item['itemtype'] = 'news'
        item['published_date'] = response.xpath('//div[@class="post-byline"]/text()').extract().pop()
        item['description'] = response.xpath('//div[@class="post-bodycopy clearfix"]').extract().pop()
        return item