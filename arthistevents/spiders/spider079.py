# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider079Spider(CrawlSpider):
    name = "spider079"
    allowed_domains = ["ucl.ac.uk"]
    start_urls = (
        'https://www.ucl.ac.uk/archaeology/calendar',
    )
    rules = [
        Rule(LinkExtractor(allow=('https://www.ucl.ac.uk/archaeology/calendar/articles/2015-16-news/.*', )), callback='parse_news_item'),
        Rule(LinkExtractor(allow=('https://www.ucl.ac.uk/archaeology/calendar/articles/2015-16-events/.*', )), callback='parse_event_item'),
    ]

    def parse_news_item(self, response):
        sel = Selector(response)

        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = sel.xpath('//*[@id="col3_content"]/div[3]/h2/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="col3_content"]/div[3]').extract().pop()
        item['url'] = response.url

        item['published_date'] = sel.xpath('//*[@id="col3_content"]/div[3]/div[1]/p/strong/text()').extract().pop()

        yield item

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="col3_content"]/div[3]/h2/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="col3_content"]/div[3]').extract().pop()
        item['url'] = response.url
        item['start_date'] = sel.xpath('//*[@id="col3_content"]/div[3]/div[1]/p[2]/span[1]/text()').extract().pop()
        item['end_date'] = sel.xpath('//*[@id="col3_content"]/div[3]/div[1]/p[2]/span[2]/text()').extract().pop()
        item['published_date'] = sel.xpath('//*[@id="col3_content"]/div[3]/div[1]/p[1]/span/text()').extract().pop()
        item['venue'] = sel.xpath('//*[@id="col3_content"]/div[3]/div[1]/p[3]/span/text()').extract().pop()

        yield item

