# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from arthistevents.items import EventItem

class Spider076Spider(CrawlSpider):
    name = "spider076"
    allowed_domains = ["medievalists.net"]
    start_urls = (
        'http://www.medievalists.net/category/conferences/',
    )

    rules = [
        Rule(LinkExtractor(allow=('http://www.medievalists.net/category/conferences/',)), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)
        for title,url,description in zip(sel.xpath('//*[@id="content"]/div/h2/a/text()') , sel.xpath('//*[@id="content"]/div/h2/a/@href'), sel.xpath('//*[@id="content"]/div/div[2]/p')):
            item = EventItem()
            item['itemtype'] = 'Conference'
            item['title'] = title.extract().strip('\r\n ')
            item['url'] = url.extract().strip('\r\n ')
            item['description'] = description.extract().strip('\r\n ')
            yield item

