# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem
import re

class Spider081Spider(scrapy.Spider):
    name = "spider081"
    allowed_domains = ["ria.ie"]
    start_urls = (
        'http://www.ria.ie/News',
    )

    def parse_news_item(self, response):
        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = response.xpath('//*[@id="content"]/div[1]/ul/li[3]/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="content"]/p').extract()[1:]
        item['url'] = response.url
        item['published_date'] = response.xpath('//*[@id="content"]/p[1]/text()[2]').extract().pop() + " " + response.xpath('//*[@id="content"]/p[1]/strong/text()').extract().pop()
        yield item

    def parse_news_list(self,response):
    	for event_url in response.xpath('//*[@id="content"]/div/div/h3/a/@href').extract():
            event_request = scrapy.Request('http://www.ria.ie' + event_url, callback=self.parse_news_item)
            yield event_request
    def parse(self, response):
        urls = []
        for i in range(18):
            urls.append(response.urljoin('?page=' + str(i)))
        for url in urls:
            request = scrapy.Request(url, callback=self.parse_news_list)
            yield request
    
