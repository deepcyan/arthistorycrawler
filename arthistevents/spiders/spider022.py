# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem

class Spider022Spider(scrapy.Spider):
    name = "spider022"
    allowed_domains = ["ria.ie"]
    start_urls = (
        'https://www.ria.ie/Events/Events-Listing.aspx',
    )

    def parse(self, response):
        for url in response.xpath('//*[@id="content"]/div/div/h3/a/@href').extract():
            yield scrapy.Request('https://www.ria.ie' + url, callback=self.parse_event_item)

    def parse_event_item(self, response):
        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] =  response.xpath('//*[@id="content"]/div[1]/ul/li[4]/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="content"]').extract().pop()
        item['url'] = response.url

        item['date'] = response.xpath('//*[@id="content"]/p[2]/text()[2]').extract().pop()[6:]
        item['venue'] = response.xpath('//*[@id="content"]/p[2]/text()[1]').extract().pop()[7:]
        yield item

