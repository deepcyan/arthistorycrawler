# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider054Spider(CrawlSpider):
    name = "spider054"
    allowed_domains = ["britishmuseum.org"]
    start_urls = (
        
        'http://www.britishmuseum.org/whats_on/events_calendar.aspx',
    )
    rules = [
        Rule(LinkExtractor(allow=('/whats_on/events_calendar/event_detail.aspx.*', )), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="mainContent"]/div[1]/h1/text()').extract()[3]
        item['description'] = sel.xpath('//*[@id="mainContent"]/div[2]').extract().pop()
        item['url'] = response.url

        item['date'] = sel.xpath('//*[@id="mainContent"]/div[1]/div[1]/p/text()[1]').extract().pop()
       
        #item['venue'] = sel.xpath('//*[@id="mainContent"]/div[1]/div[1]/p/text()[5]').extract()

        yield item