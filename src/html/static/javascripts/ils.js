angular
    .module("ils", [
        'ui.bootstrap',
        'angular-loading-bar',
        'ngCookies',
        'ngThriftService',
        'ngLodash',
        'ils.config',
        'ils.routes',
        'ils.controllers',
        'ils.services'
    ]);

angular.module('ils.routes', ['ngRoute']);
angular.module('ils.config', []);
angular.module('ils.controllers', []);
angular.module('ils.services', ['toaster', 'ngAnimate']);