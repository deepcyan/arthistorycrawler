package com.sayandeep.arthistevents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sayandeep.arthistevents.thrift.Item;
import com.sayandeep.arthistevents.thrift.ItemType;

public class ItemTypeAnnotator {


  private static Pattern REGEX_SCHOOL = Pattern.compile("school", Pattern.CASE_INSENSITIVE);
  private static Pattern REGEX_EVENT = Pattern.compile("event");
  private static Pattern REGEX_LECTURE = Pattern.compile("lecture");
  private static Pattern REGEX_CONFERENCE = Pattern.compile("conference");
  private static Pattern REGEX_CFP = Pattern.compile("call for papers");
  private static Pattern REGEX_SEMINAR = Pattern.compile("seminar");
  private static Pattern REGEX_NEWS = Pattern.compile("news");
  private static Map<ItemType, Pattern> regexes;

  public ItemTypeAnnotator() {
    regexes = new HashMap<ItemType, Pattern>();
    regexes.put(ItemType.EVENT, REGEX_EVENT);
    regexes.put(ItemType.CFP, REGEX_CFP);
    regexes.put(ItemType.LECTURE, REGEX_LECTURE);
    regexes.put(ItemType.NEWS, REGEX_NEWS);
    regexes.put(ItemType.SEMINAR, REGEX_SEMINAR);
    regexes.put(ItemType.SCHOOL, REGEX_SCHOOL);
    regexes.put(ItemType.EVENT, REGEX_CONFERENCE);
  }

  public ItemType annotate(Item item) {
    if (item.getType() == ItemType.UNKNOWN) {
      String itemTypeString = item.getJson().get("itemtype");
      String completeText = item.getJson().toString();

      ArrayList<ItemType> possibleTypes = new ArrayList<ItemType>();
      for (Entry<ItemType, Pattern> possibleTypePair : regexes.entrySet()) {
        Matcher matcher = possibleTypePair.getValue().matcher(completeText);
        while (matcher.find()) {
          // System.out.println("possible: " + possibleTypePair.getKey());
          possibleTypes.add(possibleTypePair.getKey());
        }
      }

      System.out.println("possibleTypes: " + possibleTypes);

      switch (itemTypeString) {
        case "event":
          return ItemType.EVENT;
        case "news":
          return ItemType.NEWS;
        default:
          return getMostProbableType(possibleTypes);
      }
    }
    return ItemType.UNKNOWN;
  }

  private ItemType getMostProbableType(ArrayList<ItemType> possibleTypes) {
    Map<ItemType, Integer> count = new HashMap<ItemType, Integer>();
    for (ItemType type : possibleTypes) {
      if (count.containsKey(type)) {
        count.put(type, count.get(type) + 1);
      } else {
        count.put(type, 0);
      }
    }
    Map.Entry<ItemType, Integer> maxEntry = null;
    for (Entry<ItemType, Integer> entry : count.entrySet()) {
      if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
        maxEntry = entry;
      }
    }
    return maxEntry.getKey();
  }

}
