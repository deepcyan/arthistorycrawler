# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import NewsItem

class Spider064Spider(scrapy.Spider):
    name = "spider014"
    allowed_domains = ["historyatgalway"]
    start_urls = (
        'https://historyatgalway.wordpress.com',
    )

    def parse(self, response):
        titles = response.xpath('//h1[@class="entry-title"]/a/text()').extract() 
        descriptions = response.xpath('//div[@class="content"]').extract()
        days = response.xpath('//span[@class="day"]/text()').extract()
        months = response.xpath('//div[@class="date"]/a/p/text()').extract()
        urls = response.xpath('//h1[@class="entry-title"]/a/@href').extract() 
        for title, day, month, description, url in zip(titles, days, months, descriptions, urls):

        	item = NewsItem()
        	item['itemtype'] = 'news'
        	item['description'] = description
        	item['url'] = url
        	item['title'] = title
        	item['published_date'] = day + " " + month
        	yield item
