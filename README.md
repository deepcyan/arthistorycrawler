   ART HISTORY EVENTS AND NEWS CRAWLER
=========================================

# To create a spider do

```
scrapy genspider <name> <domain>
```

For instance to create a spider named `spider003` for domain `http://www.tcd.ie/Medieval_Renaissance/`

run

```
scrapy genspider spider003 tcd.ie
```


# Run

To run a spider do

```
scrapy crawl <spider_id>
```

For example to run `spider002` for Maynooth University Centre for Irish Cultural Heritage do

```
scrapy crawl spider002
```


# Test

To test a spider with a given URL do

```
scrapy parse --spider=myspider -c parse_item -d 2 <item_url>
```

For instance, to test `spider002` with URL https://www.maynoothuniversity.ie/history/events/catholic-historical-society-ireland-annual-conference, do

```
scrapy parse --spider=spider001 -c parse_events_item -d 2 https://www.maynoothuniversity.ie/history/events/catholic-historical-society-ireland-annual-conference
```


# Setup MySQL/MariaDB

```
mysql -u root -p
create database arthistevents;
CREATE USER 'arthisteventsuser'@'localhost' IDENTIFIED BY 'cf8-f3c94fd11';
USE arthistevents;
GRANT ALL ON arthistevents.* TO 'arthisteventsuser'@'localhost';
source events.sql
```


# Configure nginx 

Add something like 

```
        location /thrift-api/v1/AHEService/ {
                rewrite ^/thrift-api/v1/(.*)$ /arthistevents/$1 break;
                proxy_pass http://127.0.0.1:8080;
        }

        location /ahe/ {
                alias /home/sayandeep/Code/arthistorycrawler/src/html/;
                # try_files $uri /index.html;
                # location ~ ^/ahe.*$ {
                #       # rewrite ^/ahe/(.*)$ /$1 break;
                #       root /home/sayandeep/Code/arthistorycrawler/src/html/;
                #       # try_files $uri /index.html;
                # }
        }
```
to your Nginx config.


# List of spiders

`spider001`
Maynooth History
https://www.maynoothuniversity.ie/history

`spider002`
Maynooth Irish Cultural Heritage
https://www.maynoothuniversity.ie/centre-irish-cultural-heritage/events

`spider003`
TCD
http://www.tcd.ie/Medieval_Renaissance/

`spider004`
TCD
https://www.tcd.ie/History_of_Art/news/

`spider005`
TCD Centre for Medieval Studies
https://www.tcd.ie/history/research/centres/medieval/seminars.php

`spider006`
Trinity Long Room Hub (2)
https://www.tcd.ie/trinitylongroomhub/events/forthcoming/

`spider007`
UCC   Archaeology
http://www.ucc.ie/en/archaeology/research/seminarslectures/

`spider008`
UCC History of Art
http://www.ucc.ie/en/arthistory/newsandevents/

`spider009`
UCD
http://www.ucd.ie/arthistory/
http://www.ucd.ie/arthistory/newsevents/publiclecturesconferencepapers/
http://www.ucd.ie/arthistory/newsevents/conferencesseminars/
http://www.ucd.ie/arthistory/newsevents/conversationsonartinireland/

`spider010`
UCD
http://www.ucd.ie/archaeology/seminarsandconferences/

`spider011`
UCD  archaeology
https://www.facebook.com/pages/UCD-School-of-Archaeology/138793279477130    

`spider012`
UCD Micheal O Cleirigh
http://www.ucd.ie/mocleirigh/
http://www.ucd.ie/mocleirigh/newsandevents/

`spider013`
UCG (NUIG)
http://www.nuigalway.ie/archaeology/News_And_Events/news_and_events_index.html
http://www.nuigalway.ie/colleges-and-schools/arts-social-sciences-and-celtic-studies/geography-archaeology/disciplines/archaeology/news-events-socialmedia/news/
http://www.nuigalway.ie/colleges-and-schools/arts-social-sciences-and-celtic-studies/geography-archaeology/disciplines/archaeology/news-events-socialmedia/events/

`spider014`
UCG (NUIG)
http://historyatgalway.wordpress.com/

`spider015`


`spider016`
The Discovery Programme
http://www.discoveryprogramme.ie/news-a-events/events.html
http://www.discoveryprogramme.ie/index.php/news-events

`spider017`
Friends of Medieval Dublin
http://friendsofmedievaldublin.wordpress.com/

`spider018`
IAAH  (Irish Assoc. of Art Historians)
https://www.facebook.com/pages/The-Irish-Association-of-Art-Historians/470348636373551?fref=ts 

`spider019`
IAAH  (Irish Assoc. of Art Historians)
http://artefactjournal.com
http://artefactjournal.com/iaah/iaah-events/

`spider020`
Institute of Archaeologists of Ireland
http://us5.campaign-archive1.com/?u=4db489b2df5de7214568c1da4&id=b394750ec7&e=f924e1aedc

`spider021`
Med-Ren-Forum
http://fmrsi.wordpress.com/events/

`spider022`
Royal Irish Academy
https://www.ria.ie/Events/Events-Listing.aspx

`spider023`
RSAI
http://rsai.ie/programme/
http://rsai.ie/lectures-2015/
http://rsai.ie/talks/
http://rsai.ie/outings-2015/

`spider024`
RSAI (Temporary series)
 https://monasticeurope.wordpress.com/2015/02/13/monastic-ireland-keynote-lecture-series-at-the-rsai/
https://monasticeurope.wordpress.com/

`spider025`
Bangor University
http://www.bangor.ac.uk/history/
http://www.bangor.ac.uk/history/events/
http://www.bangor.ac.uk/history/conferences.php.en
http://www.bangor.ac.uk/history/history-seminars.php.en
http://www.bangor.ac.uk/history/archaeology-seminars.php.en

`spider026`
British School at Rome
http://www.bsr.ac.uk
http://www.bsr.ac.uk/news
http://www.bsr.ac.uk/news/italy-events
http://www.bsr.ac.uk/news/london

`spider027`
Cambridge Colloquium in Anglo-Saxon
 Norse and Celtic [check 'Events' in 'Department' drop-down menu]"
http://www.asnc.cam.ac.uk/ccasnc/
http://www.asnc.cam.ac.uk/events/index.htm

`spider028`
Cardiff University   
http://www.cardiff.ac.uk/medievalsocietyandculture/index.html
http://www.cardiff.ac.uk/medievalsocietyandculture/events/index.html

`spider029`
Courtauld Institute
http://www.courtauld.ac.uk/index.shtml
http://courtauld.ac.uk/support/join-us
http://courtauld.ac.uk/gallery/what-on
http://courtauld.ac.uk/gallery/what-on/calendar
http://courtauld.ac.uk/whats-on
http://courtauld.ac.uk/research/research-forum/events

`spider030`
Durham University
https://www.dur.ac.uk/imems/
https://www.dur.ac.uk/imems/events/
https://www.dur.ac.uk/imems/events/lectures/
https://www.dur.ac.uk/imems/events/seminarseries/
https://www.dur.ac.uk/imems/events/conferences/
https://www.dur.ac.uk/imems/memsa/events/
https://www.dur.ac.uk/imems/events/other/

`spider031`
Edinburgh College of Art
http://www.eca.ed.ac.uk/
http://www.eca.ed.ac.uk/eca-home/news-events

`spider032`
Paul Mellon Centre
http://www.paul-mellon-centre.ac.uk/
http://www.paul-mellon-centre.ac.uk/whats-on/news
http://www.paul-mellon-centre.ac.uk/whats-on/forthcoming

`spider033`
University of Cambridge Art History/ 
http://www.hoart.cam.ac.uk/events  
http://www.hoart.cam.ac.uk/aboutthedept/events/Events
http://www.hoart.cam.ac.uk/aboutthedept/events/Events/previous

`spider034`
University of Cambridge CLANS
http://www.crassh.cam.ac.uk/programmes/late-antiquity-network-seminar-clans
http://www.crassh.cam.ac.uk/events/

`spider035`,"University of East Anglia
 Sainsbury Centre of Visual Arts"
http://scva.ac.uk/whats-on/events
http://scva.ac.uk/whats-on/events/category/lectures-symposia-and-training
http://scva.ac.uk/whats-on/events/category/workshops
http://scva.ac.uk/whats-on/events/category/friends-events
http://scva.ac.uk/whats-on/events/category/films-and-performances
http://scva.ac.uk/whats-on/events/category/family-friendly
http://scva.ac.uk/whats-on/events/category/special-events
http://scva.ac.uk/whats-on/events/category/offsite

`spider036`
University of Edinburgh
http://www.ed.ac.uk/schools-departments/history-classics-archaeology/archaeology/news-events/events/overview
http://www.ed.ac.uk/history-classics-archaeology/news-events/news
http://www.ed.ac.uk/history-classics-archaeology/archaeology/news-events/events
http://www.ed.ac.uk/history-classics-archaeology/archaeology/news-events/news

`spider037`
University of Edinburgh History of Art
http://www.eca.ed.ac.uk/history-of-art/news-events/history-of-art-research-seminars

`spider038`
University of Edinburgh
http://www.ed.ac.uk/schools-departments/history-classics-archaeology/centre-medieval-renaissance/news-events/seminars/medieval-renaissance-seminar
http://www.ed.ac.uk/history-classics-archaeology/centre-medieval-renaissance/news-events/seminars/medieval-renaissance-seminar

`spider039`
University of Edinburgh archaeology
http://www.ed.ac.uk/schools-departments/history-classics-archaeology/archaeology/news-events/events/archaeology-seminars
http://www.ed.ac.uk/history-classics-archaeology/archaeology/news-events/events/archaeology-seminars

`spider040`
University of Edinburgh history
http://www.ed.ac.uk/schools-departments/history-classics-archaeology/news-events/events/celtic-classics-214/history
http://www.ed.ac.uk/history-classics-archaeology/news-events/events

`spider041`,"UCl Scandinavian Studies School of European Languages
 Culture and Society"
http://www.ucl.ac.uk/scandinavian-studies/vikings/medieval_seminar
http://www.ucl.ac.uk/selcs/scandinavian-studies/scandinavian-studies-news-viewer
http://www.ucl.ac.uk/selcs/scandinavian-studies/scandinavian-studies-events-viewer

`spider042`
UCL History of Art
http://www.ucl.ac.uk/art-history
http://www.ucl.ac.uk/art-history/news-events
http://www.ucl.ac.uk/silva/art-history/hoa-news-events-viewer

`spider043`
UCl Medieval and Renaissance Studies
http://www.ucl.ac.uk/mars/seminars-lectures

`spider044`
University of Exeter art history
http://humanities.exeter.ac.uk/arthistory/
http://humanities.exeter.ac.uk/arthistory/events/
http://humanities.exeter.ac.uk/arthistory/news/

`spider045`
University of Exeter's Centre for Medieval Studies
http://humanities.exeter.ac.uk/history/research/centres/medieval/newsandevents/
http://humanities.exeter.ac.uk/history/research/centres/medieval/newsandevents/news/
http://humanities.exeter.ac.uk/history/research/centres/medieval/newsandevents/seminars/
http://humanities.exeter.ac.uk/history/research/centres/medieval/newsandevents/events/

`spider046`
University of St. Andrews
http://saims.wp.st-andrews.ac.uk/seminars-2/

`spider047`
University of Glasgow
http://www.gla.ac.uk/schools/cca/research/instituteofarthistory/#tabs=3
http://www.gla.ac.uk/schools/cca/research/instituteofarthistory/#/lecturesandevents

`spider048`
University of Glasgow
http://www.gla.ac.uk/schools/critical/research/researchcentresandnetworks/mrsg/
http://www.gla.ac.uk/schools/critical/research/researchcentresandnetworks/mrsg/events/

`spider049`
University of Leicester
http://www2.le.ac.uk/departments/arthistory/events

`spider050`
University of York
http://www.york.ac.uk/history-of-art/
http://www.york.ac.uk/history-of-art/news-and-events/

`spider051`
University of York
http://www.york.ac.uk/medieval-studies/
http://www.york.ac.uk/medieval-studies/events/
http://www.york.ac.uk/medieval-studies/events/archive/

`spider052`
Victoria and albert Museum
http://www.vam.ac.uk/page/c/conferences-and-symposia/
http://www.vam.ac.uk/whatson/

`spider053`
British Archaeological Association
http://thebaa.org/meetings-events/

`spider054`
British Museum events (up-to date for about a month ahead)
http://www.britishmuseum.org/whats_on/events_calendar.aspx

`spider055`
Society of Antiquaries of London
http://www.sal.org.uk/events/

`spider056`
Early Book Society
http://www.nyu.edu/projects/EBS/
http://www.nyu.edu/projects/EBS/Announcements.html

`spider057`
Early Book Society (Facebook version)
https://www.facebook.com/pages/Early-Book-Society/191172517607926

`spider058`
ISAS
http://www.isasweb.net/
http://www.isasweb.net/events.html
http://www.isasweb.net/conf.html

`spider059`
Institute of Historical Research (IHR)
http://www.history.ac.uk/events/seminars/113
http://www.history.ac.uk/events/diary

`spider060`
IHR contd
http://www.history.ac.uk/events/seminars/127 

`spider061`
IHR contd
http://www.history.ac.uk/events/seminars/128

`spider062`
Leeds University medieval online 
http://www.leeds.ac.uk/ims/med_online/calendar2015.html

`spider063`
Lyminge Archaeologogical Project
http://www.lymingearchaeology.org/
http://www.lymingearchaeology.org/conference-2015/conference-2015-timetable/
http://www.lymingearchaeology.org/exhibitions-and-events/

`spider064`
Dakam
http://www.dakam.org/#!news/cx3a
http://dakam-rc.wix.com/dakamconferences

`spider065`
Institut national d'histoire de l'art
http://www.inha.fr/fr/index.html
http://www.inha.fr/fr/formations-carrieres/actualites-professionnelles/appels-a-contributions.html

`spider066`,"International Medieval Society, Paris      Société Internationale des Médiévistes
 Paris"
www.ims-paris.org

`spider067`
L'Association des professeurs d'archéologie et d'histoire de l'art des universités (APAHAU)
http://blog.apahau.org

`spider068`
Pontifical
http://www.pims.ca/academics/calendar-of-events/11-2014
http://www.pims.ca/academics/calendar-of-events/

`spider069`


`spider070`
University of Notre Dame medieval
http://medieval.nd.edu/events-and-calendar/
http://medieval.nd.edu/events/

`spider071`


`spider072`
College Art Association
http://www.collegeart.org
http://www.collegeart.org/news/

`spider073`
International Centre for Med. Art
http://medievalart.org
http://www.medievalart.org/community-news/

`spider074`
Haskins Society
http://www.haskinssociety.org
http://www.haskinssociety.org/page-1603776

`spider075`
An events blog covering 400-1100AD
http://heroic95.rssing.com/chan-13714589/all_p1.html
http://heroic95.rssing.com/chan-13714589/all_p2.html
http://heroic95.rssing.com/chan-13714589/all_p3.html
http://heroic95.rssing.com/chan-13714589/all_p4.html
http://heroic95.rssing.com/chan-13714589/all_p5.html
http://heroic95.rssing.com/chan-13714589/all_p6.html
http://heroic95.rssing.com/chan-13714589/all_p7.html
http://heroic95.rssing.com/chan-13714589/all_p8.html
http://heroic95.rssing.com/chan-13714589/all_p9.html
http://heroic95.rssing.com/chan-13714589/all_p10.html
http://heroic95.rssing.com/chan-13714589/all_p11.html

`spider076`
Medievalists.net
http://www.medievalists.net/category/conferences/
http://www.medievalists.net/category/news/

`spider077`
MEDFEST  Medieval Conferences  
http://members.efn.org/~acd/medfest.html

`spider078`
SAS UCL
http://events.sas.ac.uk/support-research/events/advanced-search
http://events.sas.ac.uk/support-research/events/advanced-search?func=results&page=&string=&department_id=0&type_id=0&aoi_id=0&from_date_Day=27&from_date_Month=10&from_date_Year=2015&until_date_Day=27&until_date_Month=10&until_date_Year=2016&order_by=start_date&submit=SEARCH

`spider079`
UCL Institute of Archaeology
https://www.ucl.ac.uk/archaeology/
https://www.ucl.ac.uk/archaeology/calendar

`spider080`
Medieval art research
http://medievalartresearch.com/events-calendar-2/
http://medievalartresearch.com/events-calendar/

`spider081`
Royal Irish Academy
www.ria.ie
