# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider052Spider(CrawlSpider):
    name = "spider052"
    allowed_domains = ["vam.ac.uk"]
    start_urls = (
        
        'http://www.vam.ac.uk/whatson/',
    )
    rules = [
        Rule(LinkExtractor(allow=('/whatson/event/.*', )), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="single-event-layout"]/div[1]/h2/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="event"]/div[2]').extract().pop()
        item['url'] = response.url

        item['start_date'] = sel.xpath('//*[@id="event"]/div[2]/ul/li[2]/div[2]/time[1]/text()').extract().pop()
        item['end_date'] = sel.xpath('//*[@id="event"]/div[2]/ul/li[2]/div[2]/time[2]/text()').extract().pop()
        item['venue'] = sel.xpath('//*[@id="event"]/div[2]/ul/li[3]/div[2]/text()').extract().pop()

        yield item