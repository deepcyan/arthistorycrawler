(function () {
    'use strict';

    angular
        .module('ils.controllers')
        .controller('LandingController', LandingController);

    LandingController.$inject = ['ILSService', 'toaster'];

    function LandingController(ILSService, toaster) {
        var vm = this;
        vm.AttributeType = AttributeType;
        vm.ItemType = ItemType;
        vm.addToCart = function(product) {
            ILSService.addToCart(product).then(function(data) {
                toaster.pop("success", "", "Added product to cart!");
            }, function(data) {
                toaster.pop("error", "", "Failed to add product to cart!");
            });
        };

        ILSService.get().then(function(data) {
            //toaster.pop("success", "", JSON.stringify(data));

            vm.items = data;
        });
    }
})();