# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider001Spider(CrawlSpider):
    name = "spider001"
    allowed_domains = ["maynoothuniversity.ie"]
    start_urls = (
        'https://www.maynoothuniversity.ie/history',
        'https://www.maynoothuniversity.ie/history/news',
        'https://www.maynoothuniversity.ie/history/events',
    )
    rules = [
        Rule(LinkExtractor(allow=('https://www.maynoothuniversity.ie/history/news/.*', )), callback='parse_news_item'),
        Rule(LinkExtractor(allow=('https://www.maynoothuniversity.ie/history/events/archive', )), callback='parse_events_archive'),
        Rule(LinkExtractor(allow=('https://www.maynoothuniversity.ie/history/events/.*', )), callback='parse_events_item'),
    ]

    def parse_news_item(self, response):
        sel = Selector(response)

        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = sel.css('#page-title::text').extract()[0].strip('\r\n ')
        item['description'] = sel.xpath('//*[starts-with(@id, "node-news-")]/div/div[2]').extract()[0].strip('\r\n ')
        item['url'] = response.url.split('?')[0]

        item['published_date'] = ''.join(sel.xpath('//*[starts-with(@id, "node-news-")]//*[@class="field-name-field-news-published-date"]//text()').extract()).strip('\r\n ')

        yield item

    def parse_events_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.css('#page-title::text').extract()[0].strip('\r\n ')
        item['description'] = sel.css('[id^=node-event-] > div.field-name-body').extract()[0].strip('\r\n ')
        item['url'] = response.url.split('?')[0]

        item['date'] = ''.join(sel.xpath('//*[starts-with(@id, "node-event-")]/div[1]/div[2]/span//text()').extract()).strip('\r\n ')
        item['venue'] = sel.xpath('//*[starts-with(@id, "node-event-")]/div[1]/div[3]/text()').extract()[1].strip('\r\n ')

        yield item

    def parse_events_archive(self, response):
        sel = Selector(response)

        for event_url in sel.xpath('//*[@id="region-content"]/div/div[2]/div/div/div/div[2]/div/div/div/h3/a/@href'):
            url = response.urljoin(event_url.extract())
            event_request = scrapy.Request(url, callback=self.parse_events_item)
            yield event_request

