# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider072Spider(CrawlSpider):
    name = "spider072"
    allowed_domains = ["collegeart.org"]
    start_urls = (
        'http://www.collegeart.org/news/',
    )
    rules = [
        Rule(LinkExtractor(allow=('http://www.collegeart.org/news/.*', )), callback='parse_news_item'),
    ]

    def parse_news_item(self, response):
        sel = Selector(response)

        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = sel.xpath('//a[@rel="bookmark"]/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="content"]/div[1]').extract().pop()
        item['url'] = response.url

        item['published_date'] = sel.xpath('//div[@class="day"]/text()').extract().pop() + ',' + sel.xpath('//div[@class="month"]/text()').extract().pop()

        yield item