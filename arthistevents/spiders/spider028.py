# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem
import re

class Spider028Spider(scrapy.Spider):
    name = "spider028"
    allowed_domains = ["cardiff.ac.uk"]
    start_urls = (
        'http://www.cardiff.ac.uk/medievalsocietyandculture/events/index.html',
    )
    def parse(self, response):
        for i in ['6','8','10']:
    	    item = EventItem()
            item['description'] = response.xpath('//*[@id="content"]/div/p['+i+']').extract().pop()
            item['url'] = response.url
            item['title'] = re.findall(u"\u2018.*?\u2019",item['description']).pop()
            item['venue'] = response.xpath('//*[@id="content"]/div/p['+i+']/u/text()').extract().pop()
            item['itemtype'] = 'event'
            
            item['date'] = response.xpath('//*[@id="content"]/div/p['+i+']/text()[1]').extract().pop()
            yield item
        
