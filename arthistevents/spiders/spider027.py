# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import GenericItem


class Spider027Spider(scrapy.Spider):
    name = "spider027"
    allowed_domains = ["asnc.cam.ac.uk"]
    start_urls = (
        'http://www.asnc.cam.ac.uk/events/index.htm',
    )

    def parse(self, response):
        titles = response.xpath('//*[@id="content"]/div/h2/text()').extract()[1:]
        descriptions = response.xpath('//*[@id="content"]/div/p').extract()[5:-2]
        for title, description in zip(titles, descriptions):
        	item = GenericItem()
        	item['url'] = response.url
        	item['itemtype'] = 'generic'
        	item['title'] = title
        	item['description'] = description
        	yield item
