# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import GenericItem

class Spider013Spider(scrapy.Spider):
    name = "spider013"
    allowed_domains = ["nuigalway.ie"]
    start_urls = (
        'http://www.nuigalway.ie/colleges-and-schools/arts-social-sciences-and-celtic-studies/geography-archaeology/disciplines/archaeology/news-events-socialmedia/news/',
    )

    def parse(self, response):
        titles = response.xpath('//*[@id="pageContentWrapper"]/div/h2/text()').extract()
        descriptions = response.xpath('//*[@id="pageContentWrapper"]/div').extract()
        for title, description in zip(titles, descriptions):
        	item = GenericItem()
        	item['itemtype'] = 'generic'
        	item['description'] = description
        	item['url'] = response.url
        	item['title'] = title
        	yield item
