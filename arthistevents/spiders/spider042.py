# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem

class Spider042Spider(scrapy.Spider):
    name = "spider042"
    allowed_domains = ["ucl.ac.uk"]
    start_urls = (
        'http://www.ucl.ac.uk/art-history/news-events',
    )

    def parse(self, response):
        urls = response.xpath('//*[@id="art-history-news-events"]/div/div/div/div/article/div/article/div/h3/a/@href').extract()
        # item = EventItem()
        # item['url'] = urls
        # yield item
        for url in urls:
        	yield scrapy.Request(url, callback=self.parse_event_item)

    def parse_event_item(self, response):
    	item = EventItem()
    	item['url'] = response.url
    	item['itemtype'] = 'event'

    	item['title'] = response.xpath('//div[@class="newsitem"]/h1/text()').extract().pop()
    	item['published_date'] = response.xpath('//div[@class="newsiteminfo"]/p[1]/span/text()').extract().pop()
    	item['start_date'] = response.xpath('//div[@class="newsiteminfo"]/p[2]/span/text()').extract().pop()
    	item['end_date'] = response.xpath('//div[@class="newsiteminfo"]/p[3]/span/text()').extract().pop()
        item['venue'] = response.xpath('//span[@class="location"]/text()').extract().pop()
        item['description'] = response.xpath('//div[@class="newsitem"]').extract().pop()
        yield item