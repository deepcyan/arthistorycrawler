package com.sayandeep.arthistevents;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.sayandeep.arthistevents.thrift.Attribute;
import com.sayandeep.arthistevents.thrift.AttributeType;
import com.sayandeep.arthistevents.thrift.Item;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.AnnotationPipeline;
import edu.stanford.nlp.pipeline.POSTaggerAnnotator;
import edu.stanford.nlp.pipeline.TokenizerAnnotator;
import edu.stanford.nlp.pipeline.WordsToSentencesAnnotator;
import edu.stanford.nlp.time.SUTime.Temporal;
import edu.stanford.nlp.time.TimeAnnotations;
import edu.stanford.nlp.time.TimeAnnotator;
import edu.stanford.nlp.time.TimeExpression;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Pair;

/**
 * Hello world!
 *
 */
public class DateTagger {

  private Properties props;
  private static AnnotationPipeline pipeline;

  public DateTagger() {
    props = new Properties();
    pipeline = new AnnotationPipeline();
    pipeline.addAnnotator(new TokenizerAnnotator(false));
    pipeline.addAnnotator(new WordsToSentencesAnnotator(false));
    pipeline.addAnnotator(new POSTaggerAnnotator(false));
    pipeline.addAnnotator(new TimeAnnotator("sutime", props));
  }

  /**
   * Example usage: java SUTimeDemo "Three interesting dates are 18 Feb 1997, the 20th of july and 4
   * days from today."
   *
   * @param args Strings to interpret
   */
  public static void main(String[] args) {

    for (String text : args) {
      Annotation annotation = new Annotation(text);
      annotation.set(CoreAnnotations.DocDateAnnotation.class, "2015-11-15");
      pipeline.annotate(annotation);
      System.out.println(annotation.get(CoreAnnotations.TextAnnotation.class));

      List<CoreMap> timexAnnsAll = annotation.get(TimeAnnotations.TimexAnnotations.class);
      for (CoreMap cm : timexAnnsAll) {
        List<CoreLabel> tokens = cm.get(CoreAnnotations.TokensAnnotation.class);
        System.out.println(cm + " [from char offset "
            + tokens.get(0).get(CoreAnnotations.CharacterOffsetBeginAnnotation.class) + " to "
            + tokens.get(tokens.size() - 1).get(CoreAnnotations.CharacterOffsetEndAnnotation.class)
            + ']' + " --> " + cm.get(TimeExpression.Annotation.class).getTemporal());
      }
      System.out.println("--");
    }
  }

  public Item annotate(Item item) {
    List<Pair<AttributeType, String>> dates = new ArrayList<Pair<AttributeType, String>>();

    for (Attribute attribute : item.getAttributes()) {
      switch (attribute.getType()) {
        case DESCRIPTION:
        case LONG_DESCRIPTION:
        case TITLE:
          dates.addAll(getDates(attribute.getValue()));
          break;
        case URL:
        case VENUE:
          break;
        case DATE:
        case DATE_END:
        case DATE_PUBLISHED:
        case DATE_START:
          dates.addAll(getDates(attribute.getType(), attribute.getValue()));
          item.attributes.remove(attribute);
        default:
          break;
      }
    }

    for (Pair<AttributeType, String> date : dates) {
      item.addToAttributes(new Attribute(date.first, date.second));
    }
    return item;
  }

  private List<Pair<AttributeType, String>> getDates(AttributeType attributeType, String text) {
    List<Pair<AttributeType, String>> dates = new ArrayList<Pair<AttributeType, String>>();
    Annotation annotation = new Annotation(text);
    annotation.set(CoreAnnotations.DocDateAnnotation.class, "2015-11-15");
    pipeline.annotate(annotation);
    System.out.println(annotation.get(CoreAnnotations.TextAnnotation.class));
    List<CoreMap> timexAnnsAll = annotation.get(TimeAnnotations.TimexAnnotations.class);
    for (CoreMap cm : timexAnnsAll) {
      Temporal temporal = cm.get(TimeExpression.Annotation.class).getTemporal();
      boolean type = false;
      try {
        type = temporal.getGranularity().toString().equals("P1D");
      } catch (Exception e) {}

      // List<CoreLabel> tokens = cm.get(CoreAnnotations.TokensAnnotation.class);
      // System.out.println("ISO: " + temporal.toISOString() + " type: "
      // + temporal.getStandardTemporalType() + " gran: " + type);
      // System.out.println(cm + " [from char offset "
      // + tokens.get(0).get(CoreAnnotations.CharacterOffsetBeginAnnotation.class) + " to "
      // + tokens.get(tokens.size() - 1).get(CoreAnnotations.CharacterOffsetEndAnnotation.class)
      // + ']' + " --> " + temporal);
      if (type) {
        dates.add(new Pair<AttributeType, String>(attributeType, temporal.toISOString()));
      }
    }
    System.out.println("--");
    return dates;
  }

  private List<Pair<AttributeType, String>> getDates(String text) {
    List<Pair<AttributeType, String>> dates = new ArrayList<Pair<AttributeType, String>>();
    Annotation annotation = new Annotation(text);
    annotation.set(CoreAnnotations.DocDateAnnotation.class, "2015-11-15");
    pipeline.annotate(annotation);
    System.out.println(annotation.get(CoreAnnotations.TextAnnotation.class));
    List<CoreMap> timexAnnsAll = annotation.get(TimeAnnotations.TimexAnnotations.class);
    for (CoreMap cm : timexAnnsAll) {
      Temporal temporal = cm.get(TimeExpression.Annotation.class).getTemporal();
      boolean type = false;
      try {
        type = temporal.getGranularity().toString().equals("P1D");
      } catch (Exception e) {}

      // List<CoreLabel> tokens = cm.get(CoreAnnotations.TokensAnnotation.class);
      // System.out.println("ISO: " + temporal.toISOString() + " type: "
      // + temporal.getStandardTemporalType() + " gran: " + type);
      // System.out.println(cm + " [from char offset "
      // + tokens.get(0).get(CoreAnnotations.CharacterOffsetBeginAnnotation.class) + " to "
      // + tokens.get(tokens.size() - 1).get(CoreAnnotations.CharacterOffsetEndAnnotation.class)
      // + ']' + " --> " + temporal);
      if (type) {
        dates.add(new Pair<AttributeType, String>(AttributeType.DATE, temporal.toISOString()));
      }
    }
    System.out.println("--");
    return dates;
  }
}
