# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from arthistevents.items import EventItem

class Spider077Spider(CrawlSpider):
    name = "spider077"
    allowed_domains = ["efn.org"]
    start_urls = (
        'http://members.efn.org/~acd/medfest.html',
    )

    rules = [
        Rule(LinkExtractor(allow=('http://members.efn.org/~acd/medfest.html',)), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)
        descriptions = sel.xpath('//p')
        descriptions.pop(26)
        for title,description in zip(sel.xpath('//h4')[1:58] , descriptions[4:61]):
            item = EventItem()
            item['itemtype'] = 'Conference'
            item['title'] = title.extract().strip('\r\n ')
            item['description'] = description.extract().strip('\r\n ')
            yield item

