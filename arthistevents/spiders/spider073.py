# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import NewsItem

class Spider073Spider(scrapy.Spider):
    name = "spider073"
    allowed_domains = ["medievalart.org"]
    start_urls = (
        'http://www.medievalart.org/',
    )

    def parse(self, response):
        descriptions = response.xpath('//div[@class="summary-excerpt"]/p/text()').extract()
        titles = response.xpath('//div[@class="summary-title"]/a/text()').extract()
        urls = response.xpath('//div[@class="summary-title"]/a/@href').extract()

        dates = response.xpath('//time/text()').extract()
        for title, url, description, date in zip(titles, urls, descriptions, dates):
        	item = NewsItem()
        	item['url'] = 'http://www.medievalart.org' + url
        	item['title'] = title
        	item['description'] = description
        	item['itemtype'] = 'news'
        	item['published_date'] = date
        	yield item
