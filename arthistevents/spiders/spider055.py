# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider055Spider(CrawlSpider):
    name = "spider055"
    allowed_domains = ["sal.org.uk"]
    start_urls = (
        
        'https://www.sal.org.uk/events/',
    )
    rules = [
        Rule(LinkExtractor(allow=('/events/.*', )), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('/html/body/div[1]/div/section/article/section[1]/h1/text()').extract().pop()
        item['description'] = sel.xpath('/html/body/div[1]/div/section/article/section[1]').extract().pop()
        item['url'] = response.url

        item['date'] = sel.xpath('/html/body/div[1]/div/section/article/header/div[2]/div[1]/text()').extract().pop()+ "," \
        + sel.xpath('/html/body/div[1]/div/section/article/header/div[2]/div[2]/div[1]/text()').extract().pop().strip()
       
        item['venue'] = sel.xpath('/html/body/div[1]/div/section/article/section[1]/text()').extract()[-1].strip()

        yield item