# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import GenericItem

class Spider016Spider(scrapy.Spider):
    name = "spider016"
    allowed_domains = ["discoveryprogramme.ie"]
    start_urls = (
        'http://www.discoveryprogramme.ie/index.php/news-events',
    )

    def parse(self, response):
        url = response.xpath('//*[@id="sidebar-2"]/div/div/div[2]/ul/li/a/@href').extract().pop()
        yield scrapy.Request('http://www.discoveryprogramme.ie' + url, callback = self.parse_event_item)

    def parse_event_item(self, response):
        titles = response.xpath('//*[@id="main"]/div/article/section/p/span/strong/text()').extract()
        descriptions = response.xpath('//*[@id="main"]/div/article/section/p').extract()[2:]
        for title, description in zip(titles, descriptions):
        	item = GenericItem()
        	item['itemtype'] = 'generic'
        	item['description'] = description
        	item['url'] = response.url
        	item['title'] = title
        	yield item
