/**
 * Created by sayandeep on 26/08/15.
 */

(function () {
    'use strict';

    angular
        .module('ils.config')
        .config(config);

    config.$inject = ['$locationProvider'];

    /**
     * @name config
     * @desc Enable HTML5 routing
     */
    function config($locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    }
})();