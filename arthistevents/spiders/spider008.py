# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from arthistevents.items import NewsItem

class Spider008Spider(CrawlSpider):
    name = "spider008"
    allowed_domains = ["ucc.ie"]
    start_urls = (
        'http://www.ucc.ie/en/arthistory/newsandevents/',
    )

    def parse(self, response):
        for url in response.xpath('//*[@id="content"]/div/div[2]/h2/a/@href').extract():
            yield scrapy.Request('http://www.ucc.ie' + url, callback=self.parse_news_item)
    def parse_news_item(self, response):
        item = NewsItem()
        item['url'] = response.url
        item['title'] = response.xpath('//*[@id="content"]/div/div[1]/h2/text()').extract().pop().strip()
        item['itemtype'] = 'news'
        item['published_date'] = response.xpath('//*[@id="content"]/div/div[3]/ul/li/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="content"]/div').extract().pop()
        return item