package com.sayandeep.arthistevents;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;

import com.sayandeep.arthistevents.thrift.AHEError;
import com.sayandeep.arthistevents.thrift.AHEErrorCode;
import com.sayandeep.arthistevents.thrift.Item;
import com.sayandeep.arthistevents.utils.HashUtil;

public class MySQLDatastore implements Datastore {

  private static final String CLASS_FOR_NAME = "com.mysql.jdbc.Driver";
  private static final String DB_CONNECTION_STRING = "jdbc:mysql://localhost:3306/arthistevents";
  private static final String DB_USER = "arthisteventsuser";
  private static final String DB_PASSWORD = "cf8-f3c94fd11";
  private Connection conn = null;

  public MySQLDatastore() {
    super();
    try {
      Class.forName(CLASS_FOR_NAME);
      conn = DriverManager.getConnection(DB_CONNECTION_STRING, DB_USER, DB_PASSWORD);
      this.setConn(conn);
    } catch (SQLException | ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {}
  }

  public Connection getConn() {
    return conn;
  }

  public void setConn(Connection conn) {
    this.conn = conn;
  }

  public void refreshConn() {
    try {
      Class.forName(CLASS_FOR_NAME);
      conn = DriverManager.getConnection(DB_CONNECTION_STRING, DB_USER, DB_PASSWORD);
      this.setConn(conn);
    } catch (SQLException | ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {}
  }

  @Override
  public void createTable() {
    // TODO Auto-generated method stub

  }

  byte[] getRow(String tableName, String id) throws ClassNotFoundException, AHEError {
    String sql = "SELECT data FROM " + tableName + " where id = ?";
    PreparedStatement stmt;
    byte[] byteArray = null;
    try {
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet != null && resultSet.next()) {
        byteArray = resultSet.getBytes(1);
        break;
      }
      if (byteArray == null) throw new AHEError().setType(AHEErrorCode.NOT_FOUND);
    } catch (SQLException e) {
      refreshConn();
      e.printStackTrace();
    }
    return byteArray;
  }

  List<byte[]> getAllRows(String tableName) throws SQLException, ClassNotFoundException {
    String sql = "SELECT data FROM " + tableName;
    PreparedStatement stmt = conn.prepareStatement(sql);
    ResultSet resultSet = stmt.executeQuery();

    List<byte[]> byteArrays = new ArrayList<byte[]>();
    while (resultSet != null && resultSet.next()) {
      byteArrays.add(resultSet.getBytes(1));
    }

    return byteArrays;
  }

  void createRow(String tableName, String id, byte[] byteArray)
      throws SQLException, ClassNotFoundException {
    PreparedStatement pst =
        conn.prepareStatement("INSERT INTO " + tableName + " (id, data) VALUES(?, ?)");

    pst.setString(1, id);
    pst.setBytes(2, byteArray);
    pst.executeUpdate();
  }

  private void putRow(String tableName, String id, byte[] byteArray) throws AHEError {
    try {
      if (conn == null) {
        refreshConn();
      }
      PreparedStatement pst =
          conn.prepareStatement("INSERT INTO " + tableName + " (id, data) VALUES(?, ?)");

      pst.setString(1, id);
      pst.setBytes(2, byteArray);
      pst.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      throw new AHEError().setType(AHEErrorCode.INTERNAL_ERROR);
    }
  }

  private void updateRow(String tableName, String id, byte[] byteArray) throws AHEError {
    try {
      PreparedStatement pst =
          conn.prepareStatement("UPDATE " + tableName + " SET data=? WHERE id=?");

      pst.setString(2, id);
      pst.setBytes(1, byteArray);
      pst.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      throw new AHEError().setType(AHEErrorCode.INTERNAL_ERROR);
    }
  }

  @Override
  public Item getItem(String id) throws AHEError {
    Item item = new Item();
    try {
      byte[] byteArray = getRow("Item", id);
      new TDeserializer().deserialize(item, byteArray);
    } catch (ClassNotFoundException | TException e) {
      e.printStackTrace();
      throw new AHEError().setType(AHEErrorCode.INTERNAL_ERROR);
    }
    return item;
  }

  @Override
  public void putItem(Item item) throws AHEError {
    TSerializer serialiser = new TSerializer();
    try {
      String hash = HashUtil.hash(item.toString());
      putRow("Item", hash, serialiser.serialize(item));
    } catch (TException e) {
      e.printStackTrace();
      throw new AHEError().setType(AHEErrorCode.INTERNAL_ERROR);
    } catch (InvalidKeyException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SignatureException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  @Override
  public void putItems(List<Item> items) throws AHEError {
    for (Item item : items) {
      putItem(item);
    }
  }

  @Override
  public List<Item> getItems() throws AHEError {
    List<Item> items = new ArrayList<Item>();
    try {
      List<byte[]> byteArrays = getAllRows("Item");
      TDeserializer deserialiser = new TDeserializer();
      for (byte[] byteArray : byteArrays) {
        Item Item = new Item();
        deserialiser.deserialize(Item, byteArray);
        items.add(Item);
      }
    } catch (ClassNotFoundException | SQLException | TException e) {
      e.printStackTrace();
      throw new AHEError().setType(AHEErrorCode.INTERNAL_ERROR);
    }

    return items;
  }

}
