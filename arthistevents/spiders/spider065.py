# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import GenericItem


class Spider065Spider(CrawlSpider):
    name = "spider065"
    allowed_domains = ["inha.fr"]
    start_urls = (
        
        'http://www.inha.fr/fr/formations-carrieres/actualites-professionnelles/appels-a-contributions.html',
    )
    rules = [
        Rule(LinkExtractor(allow=('http://www.inha.fr/fr/formations-carrieres/actualites-professionnelles/appels-a-contributions/.*', )), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)

        item = GenericItem()
        item['itemtype'] = 'generic'
        item['title'] = sel.xpath('//*[@id="ametys-cms-zone-default-item-N1005C"]/div/h1/span[1]/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="ametys-cms-zone-default-item-N1005C"]/div').extract().pop()
        item['url'] = response.url

        yield item