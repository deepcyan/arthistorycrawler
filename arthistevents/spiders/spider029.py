# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import NewsItem

class Spider029Spider(scrapy.Spider):
    name = "spider029"
    allowed_domains = ["courtauld.ac.uk"]
    start_urls = (
        'http://courtauld.ac.uk/news',
    )

    def parse(self, response):
        urls = response.xpath('/html/body/div[1]/div[1]/div[4]/div/div[2]/ul/li/div/p[4]/a/@href').extract()
        for url in urls:
        	yield scrapy.Request(url, callback = self.parse_news_item)
    def parse_news_item(self, response):
    	item = NewsItem()
    	item['url'] = response.url
    	item['title'] = response.xpath('/html/body/div/div[1]/div[3]/div[2]/div[1]/h1/text()').extract().pop()
    	item['description'] = response.xpath('/html/body/div/div[1]/div[3]/div[2]').extract().pop()
    	item['itemtype'] = 'news'
    	item['published_date'] = response.xpath('/html/body/div/div[1]/div[3]/div[2]/div[1]/span/text()').extract().pop()
    	yield item

