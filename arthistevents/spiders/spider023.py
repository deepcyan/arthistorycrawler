# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, GenericItem
import re


class Spider023Spider(CrawlSpider):
    name = "spider023"
    allowed_domains = ["rsai.ie"]
    start_urls = (
        'http://rsai.ie/programme/',
    )
    rules = [
        #Rule(LinkExtractor(allow=('http://rsai.ie/programme/'))),
        Rule(LinkExtractor(allow=('http://rsai.ie/lectures-2015/', )), callback='parse_lectures_item'),
        Rule(LinkExtractor(allow=('http://rsai.ie/talks/', )), callback='parse_talks_item'),
        Rule(LinkExtractor(allow=('http://rsai.ie/outings-2015/', )), callback='parse_outings_item'),
    ]

    def parse_lectures_item(self, response):
        descriptions = response.xpath('//*[@id="post-88"]/div/div/ul/li/text()').extract()
        descriptions.pop(5)
        for description in descriptions:
            item = NewsItem()
            item['itemtype'] = 'news'
            item['title'] = re.findall(r"'.*?'",description)
            item['description'] = description
            item['url'] = response.url
            item['published_date'] = re.findall(r"\d{1,2}.*?2015",description)
            yield item

    def parse_talks_item(self, response):
        descriptions = response.xpath('//*[@id="post-181"]/div/div/ul/li/text()').extract()
        
        for description in descriptions:
            item = NewsItem()
            item['itemtype'] = 'news'
            item['title'] = re.findall(r"‘.*?’",description)
            item['description'] = description
            item['url'] = response.url
            item['published_date'] = re.findall(r"\d{1,2}.*?2015",description)
            yield item

    def parse_outings_item(self, response):
        item = GenericItem()
        item['itemtype'] = 'generic'
        item['title'] = 'Outing A'
        item['description'] = response.xpath('//*[@id="post-192"]/div/div/p[2]/text()').extract().pop()
        item['url'] = response.url
        yield item
        item = GenericItem()
        item['itemtype'] = 'generic'
        item['title'] = 'Outing B'
        item['description'] = response.xpath('//*[@id="post-192"]/div/div/p[4]/text()').extract().pop()
        item['url'] = response.url
        yield item

