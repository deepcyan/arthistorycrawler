/**
 * Created by sayandeep on 26/08/15.
 */

(function () {
    'use strict';

    angular
        .module('ils.routes')
        .config(config);

    config.$inject = ['$routeProvider'];

    /**
     * @name config
     * @desc Define valid application routes
     */
    function config($routeProvider) {
        $routeProvider.when('/ahe/', {
            controller: 'LandingController',
            controllerAs: 'vm',
            templateUrl: '/ahe/static/templates/landing.html'
        });
    }
})();