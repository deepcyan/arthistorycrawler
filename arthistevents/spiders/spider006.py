# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem
from arthistevents.items import GenericItem
import codecs


months = [' January 2013',
' February 2013',
' March 2013',
' April 2013',
' May 2013',
' June 2013',
' July 2013',
' August 2013',
' September 2013',
' October 2013',
' November 2013',
' December 2013']

class Spider006Spider(scrapy.Spider):
    name = "spider006"
    allowed_domains = ["tcd.ie/trinitylongroomhub/"]
    start_urls = (
        'http://www.tcd.ie/trinitylongroomhub/events/forthcoming/',
    )

    items = []
    def parse(self, response):
        sel = Selector(response)
        
        txt = sel.css('#main-content').extract()[0]        
        txtarray = txt.split('\n')
        
        buffer = ''
        title = ''
        for a in txtarray:
         
         if any(word in a for word in months ):
            
            if( buffer != '' ):
               item = GenericItem()
               item['itemtype'] = 'generic'
               item['description'] = buffer
               item['title'] = title
               yield item
            buffer = a	
            title = a
         else:
			buffer = buffer + a
	
         
          
        pass



