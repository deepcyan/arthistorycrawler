# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem


class Spider033Spider(scrapy.Spider):
    name = "spider033"
    allowed_domains = ["hoart.cam.ac.uk"]
    start_urls = (
        'http://www.hoart.cam.ac.uk/aboutthedept/events',
    )

    def parse(self, response):
        titles = response.xpath('//*[@id="content-core"]/div/dl/dt/span[1]/a/text()').extract()
        descriptions = response.xpath('//*[@id="content-core"]/div/dl/dt').extract()
        dates = response.xpath('//*[@id="content-core"]/div/dl/dt/span[2]/span[1]/abbr[1]/text()').extract()
        venues = response.xpath('//*[@id="content-core"]/div/dl/dt/span[2]/span[2]/span/text()').extract()
        for title, description, date, venue in zip(titles, descriptions, dates, venues):
        	item = EventItem()
        	item['url'] = response.url
        	item['itemtype'] = 'event'
        	item['title'] = title
        	item['description'] = description
        	item['date'] = date
        	item['venue'] = venue
        	yield item

