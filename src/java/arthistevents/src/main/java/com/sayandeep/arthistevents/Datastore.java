package com.sayandeep.arthistevents;

import java.util.List;

import com.sayandeep.arthistevents.thrift.AHEError;
import com.sayandeep.arthistevents.thrift.Item;

public interface Datastore {
  void createTable();

  void putItem(Item item) throws AHEError;

  Item getItem(String id) throws AHEError;

  void putItems(List<Item> items) throws AHEError;

  List<Item> getItems() throws AHEError;
}
