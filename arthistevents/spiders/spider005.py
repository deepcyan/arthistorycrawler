# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem
from arthistevents.items import GenericItem
import logging
import codecs

logger = logging.getLogger('mycustomlogger')

months = ['January 2014',
' February 2014',
' March 2014',
' April 2014',
' May 2014',
' June 2014',
' July 2014',
' August 2014',
' September 2014',
' October 2014',
' November 2014',
' December 2014',
' January 2013',
' February 2013',
' March 2013',
' April 2013',
' May 2013',
' June 2013',
' July 2013',
' August 2013',
' September 2013',
' October 2013',
' November 2013',
' December 2013',
'January 2013',
' February 2012',
' March 2012',
' April 2012',
' May 2012',
' June 2012',
' July 2012',
' August 2012',
' September 2012',
' October 2012',
' November 2012',
' December 2012',
' January 2011',
' February 2011',
' March 2011',
' April 2011',
' May 2011',
' June 2011',
' July 2011',
' August 2011',
' September 2011',
' October 2011',
' November 2011',
' December 2011']

class Spider005Spider(scrapy.Spider):
    name = "spider005"
    allowed_domains = ["tcd.ie/history"]
    start_urls = (
        'http://www.tcd.ie/history/news',
    )

    rules = [
        Rule(LinkExtractor(allow=('http://www.tcd.ie/history/news/.*',)), callback='parse_news_items'),
    ]

    def parse_news_items(self, response):
        
        pass

    def parse(self, response):
        sel = Selector(response)        
        txt = sel.css('#main-content').extract()[0]
        txtarray = txt.split('\n')
        buffer = ''
		
        for a in txtarray:
         if any(word in a for word in months ):
            if( buffer != '' ):
               item = GenericItem()
               item['itemtype'] = 'generic'
               item['description'] = buffer               
               yield item
            buffer = a	
         else:
			buffer = buffer + a
	
        
          
        pass
