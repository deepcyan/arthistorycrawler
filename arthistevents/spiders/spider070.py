# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from arthistevents.items import EventItem

class Spider070Spider(CrawlSpider):
    name = "spider070"
    allowed_domains = ["medieval.nd.edu"]
    start_urls = (
        'http://medieval.nd.edu/events/',
    )

    def parse(self, response):
  
        item = EventItem()
        item['url'] = response.url
        
        item['itemtype'] = 'event'
        item['date'] = response.xpath('//*[@id="event_34525"]/h2/time[1]/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="event_34525"]').extract().pop()
        item['venue'] = response.xpath('//*[@id="event_34525"]/h3/text()').extract().pop()
        yield item
        item = EventItem()
        item['url'] = response.url
        
        item['itemtype'] = 'event'
        item['date'] = response.xpath('//*[@id="event_34526"]/h2/time[1]/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="event_34526"]').extract().pop()
        item['venue'] = response.xpath('//*[@id="event_34526"]/h3/text()').extract().pop()
        yield item