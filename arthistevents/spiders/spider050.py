# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider050Spider(CrawlSpider):
    name = "spider050"
    allowed_domains = ["york.ac.uk"]
    start_urls = (
        'http://www.york.ac.uk/history-of-art/news-and-events/',
    )
    rules = [
        Rule(LinkExtractor(allow=('/history-of-art/news-and-events/news/.*', )), callback='parse_news_item'),
        Rule(LinkExtractor(allow=('/history-of-art/news-and-events/events/.*', )), callback='parse_event_archive'),
        
    ]

    def parse_news_item(self, response):
        sel = Selector(response)

        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = sel.xpath('//*[@id="mdcolumn"]/h1/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="mdcolumn"]').extract().pop()
        item['url'] = response.url

        item['published_date'] = sel.xpath('//*[@id="mdcolumn"]/p[1]/text()').extract().pop()

        yield item

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="event"]/h1/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="event"]').extract().pop()
        item['url'] = response.url

        item['date'] = sel.xpath('//*[@id="event"]/p[1]/abbr/text()').extract().pop()
        item['venue'] = sel.xpath('//*[@id="event"]/p[8]/span/text()').extract()[1].strip('\r\n ')

        yield item