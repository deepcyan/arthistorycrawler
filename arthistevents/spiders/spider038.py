# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem
import re
class Spider038Spider(scrapy.Spider):
    name = "spider038"
    allowed_domains = ["ed.ac.uk"]
    start_urls = (
        'http://www.ed.ac.uk/history-classics-archaeology/centre-medieval-renaissance/news-events/seminars/medieval-renaissance-seminar',
    )

    def parse(self, response):
        descriptions = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr').extract()
        dates = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[1]/text()').extract()
        venues = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[4]').extract()
        titles = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[3]').extract()
        for date, venue, title, description in zip(dates, venues, titles, descriptions):
        	item = EventItem()
        	item['itemtype'] = 'event'
        	item['url'] = response.url
        	item['date'] = date
        	item['venue'] = venue

        	item['title'] = "|".join(re.findall(r"(?<=>).*?(?=<)", title))
        	item['description'] = description
        	yield item