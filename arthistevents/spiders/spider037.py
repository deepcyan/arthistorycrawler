# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem

class Spider037Spider(scrapy.Spider):
    name = "spider037"
    allowed_domains = ["eca.ed.ac.uk"]
    start_urls = (
        'http://www.eca.ed.ac.uk/history-of-art/news-events/history-of-art-research-seminars',
    )

    def parse(self, response):
        urls = response.xpath('//*[@id="block-eca-page-media-content-media"]/div/div/div/div/p/a/@href').extract()
        for url in urls:
        	yield scrapy.Request(url, callback=self.parse_event_item)
    def parse_event_item(self, response):
    	item = EventItem()
    	item['url'] = response.url
    	item['itemtype'] = 'event'
    	item['description'] = response.xpath('/html/body/div[1]/div[2]/div').extract().pop()
    	item['title'] = response.xpath('/html/body/div[1]/div[1]/h1/text()').extract().pop()
    	item['date'] = response.xpath('//*[@id="block-eca-page-media-content-media"]/div/p[1]/text()[1]').extract().pop()
    	item['venue'] = response.xpath('//*[@id="block-eca-page-media-content-media"]/div/p[1]/text()[3]').extract().pop()
    	yield item