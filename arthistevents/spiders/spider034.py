# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider034Spider(CrawlSpider):
    name = "spider034"
    allowed_domains = ["crassh.cam.ac.uk"]
    start_urls = (
        'http://www.crassh.cam.ac.uk/events/',
    )
    rules = [
        Rule(LinkExtractor(allow=('/events/\d{5}', )), callback='parse_news_item'),
    ]

    def parse_news_item(self, response):
        sel = Selector(response)

        item = NewsItem()
        item['itemtype'] = 'news'
        item['title'] = sel.xpath('//*[@id="events"]/div[1]/div/div[1]/div/h1/text()').extract()[0]
        item['description'] = sel.xpath('//*[@id="tabs"]/div[2]/div[1]').extract()[0]
        item['url'] = response.url

        item['published_date'] = sel.xpath('//*[@id="events"]/div[1]/div/div[2]/div[1]/h4/text()').extract().pop().strip('\n\t')

        yield item