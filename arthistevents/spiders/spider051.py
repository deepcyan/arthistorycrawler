# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider051Spider(CrawlSpider):
    name = "spider051"
    allowed_domains = ["york.ac.uk"]
    start_urls = (
        
        'http://www.york.ac.uk/medieval-studies/events/archive/',
    )
    rules = [
        Rule(LinkExtractor(allow=('/medieval-studies/events/archive.*', )), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="event"]/h1/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="event"]').extract().pop()
        item['url'] = response.url

        item['date'] = sel.xpath('//*[@id="event"]/p[1]/abbr/text()').extract().pop()
        item['venue'] = sel.xpath('//span[@class="location"]/text()').extract().pop()

        yield item