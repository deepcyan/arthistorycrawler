# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import Rule, CrawlSpider
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from arthistevents.items import NewsItem, EventItem


class Spider078Spider(CrawlSpider):
    name = "spider078"
    allowed_domains = ["sas.ac.uk"]
    start_urls = (
        'http://events.sas.ac.uk/support-research/events/advanced-search?func=results&page=&string=&department_id=0&type_id=0&aoi_id=0&from_date_Day=27&from_date_Month=10&from_date_Year=2015&until_date_Day=27&until_date_Month=10&until_date_Year=2016&order_by=start_date&submit=SEARCH',
    )
    rules = [
        Rule(LinkExtractor(allow=('http://events.sas.ac.uk/support-research/events/view/.*', )), callback='parse_events_item'),
    ]

    

    def parse_events_item(self, response):
        sel = Selector(response)

        item = EventItem()
        item['itemtype'] = 'event'
        item['title'] = sel.xpath('//*[@id="content"]/h1/text()').extract().pop()
        item['description'] = sel.xpath('//*[@id="content"]/dl[1]').extract().pop()
        item['url'] = response.url

        item['date'] = sel.xpath('//*[@id="content"]/p/strong/text()').extract().pop()
        item['venue'] = sel.xpath('//*[@id="content"]/dl[2]/dd[1]/text()').extract().pop()

        yield item


