# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import EventItem

class Spider030Spider(scrapy.Spider):
    name = "spider030"
    allowed_domains = ["dur.ac.uk"]
    start_urls = (
        'https://www.dur.ac.uk/imems/events/',
    )

    def parse(self, response):
        urls = response.xpath('//*[@id="content140562"]/div[1]/div/ul/li/a[1]/@href').extract()
        for url in urls:
        	# item = EventItem()
        	# item['url'] = 'https://www.dur.ac.uk/imems/events' + url[1:]
        	# yield item
            yield scrapy.Request('https://www.dur.ac.uk/imems/events' + url[1:], callback = self.parse_event_item)
    def parse_event_item(self,response):
        item = EventItem()
        item['url'] = response.url
        item['itemtype'] = 'event'
        item['title'] = response.xpath('//*[@id="content140562"]/div/h2/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="content140562"]/div').extract().pop()
        info = response.xpath('//*[@id="content140562"]/div/div/text()').extract().pop()
        item['date'] = info.split(',')[0]
        item['venue'] = "".join(info.split(',')[-3:])
        yield item

