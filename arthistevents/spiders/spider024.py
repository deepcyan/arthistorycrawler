# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from arthistevents.items import NewsItem

class Spider024Spider(CrawlSpider):
    name = "spider024"
    allowed_domains = ["monasticeurope.wordpress.com"]
    start_urls = (
        'https://monasticeurope.wordpress.com',
    )

    def parse(self, response):
        dates = response.xpath('//time[@class="entry-date published"]/text()').extract()
        titles = response.xpath('//h1[@class="entry-title"]/a/text()').extract()[2:-1]
        urls =  response.xpath('//h1[@class="entry-title"]/a/@href').extract()[2:-1]
        for date, title, url in zip(dates, titles, urls):
            item = NewsItem()
            item['url'] = url
            item['title'] = title
            item['itemtype'] = 'news'
            item['published_date'] = date
            item['description'] = date + ',' + title
            yield item