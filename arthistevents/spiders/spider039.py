# -*- coding: utf-8 -*-
import scrapy
import re
from arthistevents.items import EventItem


class Spider039Spider(scrapy.Spider):
    name = "spider039"
    allowed_domains = ["ed.ac.uk"]
    start_urls = (
        'http://www.ed.ac.uk/history-classics-archaeology/archaeology/news-events/events/archaeology-seminars',
    )

    def parse(self, response):
    	info = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/p[3]/strong/text()').extract().pop()
        venue = info + response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/p[3]/text()[2]').extract().pop()
        dates = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[1]/text()').extract()
        titles = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[3]/text()').extract()
        descriptions = response.xpath('//*[@id="block-system-main"]/div/div/div/div/div/div/div/div/div/table/tbody/tr').extract()

        for date,title,description in zip(dates, titles, descriptions):
    	    item = EventItem()
    	    item['url'] = response.url
    	    item['date'] = date
    	    item['title'] = title
    	    item['description'] = description
    	    item['itemtype'] = 'event'
            item['venue'] = venue
            yield item