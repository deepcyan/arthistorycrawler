# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from arthistevents.items import EventItem

class Spider026Spider(CrawlSpider):
    name = "spider026"
    allowed_domains = ["bangor.ac.uk"]
    start_urls = (
        'http://www.bangor.ac.uk/history/events/',
    )

    rules = [
        Rule(LinkExtractor(allow=('http://www.bangor.ac.uk/history/events/$',)), callback='parse_event_item'),
    ]

    def parse_event_item(self, response):
        sel = Selector(response)
        for title,url,description in zip(sel.xpath('//*[@id="contents"]/h2/a/text()') , sel.xpath('//*[@id="contents"]/h2/a/@href'), sel.xpath('//*[@id="contents"]/p')[1:]):
            item = EventItem()
            item['itemtype'] = 'event'
            item['title'] = title.extract().strip('\r\n ')
            item['url'] = url.extract().strip('\r\n ')
            item['description'] = description.extract().strip('\r\n ')
            yield item

