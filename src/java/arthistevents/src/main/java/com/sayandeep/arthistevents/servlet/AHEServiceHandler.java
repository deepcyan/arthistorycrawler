package com.sayandeep.arthistevents.servlet;

import java.util.List;

import org.apache.thrift.TException;

import com.sayandeep.arthistevents.Datastore;
import com.sayandeep.arthistevents.MySQLDatastore;
import com.sayandeep.arthistevents.thrift.AHEError;
import com.sayandeep.arthistevents.thrift.AHEService.Iface;
import com.sayandeep.arthistevents.thrift.Item;

public class AHEServiceHandler implements Iface {

  private Datastore datastore;

  public AHEServiceHandler() {
    datastore = new MySQLDatastore();
  }

  @Override
  public List<Item> getAll() throws AHEError, TException {
    System.out.println("getAll");
    List<Item> items = datastore.getItems();
    return items;
  }

}
