# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import NewsItem

class Spider044Spider(scrapy.Spider):
    name = "spider044"
    allowed_domains = ["humanities.exeter.ac.uk"]
    start_urls = (
        'http://humanities.exeter.ac.uk/arthistory/news/',
    )

    def parse(self, response):
        urls = response.xpath('//*[@id="contentmainnav"]/h2/a/@href').extract()
        # item = NewsItem()
        # item['url'] = urls
        # yield item
        for url in urls:
        	yield scrapy.Request('http://humanities.exeter.ac.uk' + url, callback = self.parse_news_item)

    def parse_news_item(self, response):
    	item = NewsItem()
    	item['url'] = response.url
    	item['itemtype'] = 'news'
    	item['title'] = response.xpath('//*[@id="contentmainnav"]/div[5]/h1/text()').extract().pop()
    	item['published_date'] = response.xpath('//*[@id="contentmainnav"]/p[1]').extract().pop()[6:]
    	item['description'] = response.xpath('//*[@id="contentmainnav"]/div[5]').extract().pop()
    	yield item
