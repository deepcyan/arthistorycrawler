# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from arthistevents.items import NewsItem

class Spider032Spider(CrawlSpider):
    name = "spider032"
    allowed_domains = ["paul-mellon-centre.ac.uk"]
    start_urls = (
        'http://www.paul-mellon-centre.ac.uk/whats-on/news',
    )

    def parse(self, response):
        for url in response.xpath('//*[@id="page1"]/div/ul/li/a/@href').extract():
            yield scrapy.Request(url, callback=self.parse_news_item)
    def parse_news_item(self, response):
        item = NewsItem()
        item['url'] = response.url
        item['title'] = response.xpath('//*[@id="title"]/div/div/h1/text()').extract().pop()
        item['itemtype'] = 'news'
        item['published_date'] = response.xpath('//*[@id="body"]/div/div[2]/ul/li/text()').extract().pop()
        item['description'] = response.xpath('//*[@id="body"]/div/div[3]').extract().pop()
        return item