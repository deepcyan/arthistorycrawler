package com.sayandeep.arthistevents;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;

import com.google.gson.stream.JsonReader;
import com.sayandeep.arthistevents.thrift.AHEError;
import com.sayandeep.arthistevents.thrift.Attribute;
import com.sayandeep.arthistevents.thrift.AttributeType;
import com.sayandeep.arthistevents.thrift.Item;

public class Indexer {
  private static ItemTypeAnnotator itemTypeAnnotator = new ItemTypeAnnotator();
  private static DateTagger dateTagger = new DateTagger();
  private static Datastore datastore;

  public static void main(String[] args) {
    // Read json file from arguments
    List<Item> items = readJson(args);
    datastore = new MySQLDatastore();

    // Parse JSON
    items = parseItems(items);
    // Deduplicate
    try {
      datastore.putItems(items);
    } catch (AHEError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static List<Item> parseItems(List<Item> items) {
    ArrayList<Item> parsedItems = new ArrayList<Item>();
    for (Item item : items) {
      parsedItems.add(parseItem(item));
      System.out.println("-");
    }
    return parsedItems;
  }

  private static Item parseItem(Item item) {
    Map<String, String> itemJson = item.getJson();
    List<Attribute> attributes = new ArrayList<Attribute>();
    for (Entry<String, String> attribute : itemJson.entrySet()) {
      switch (attribute.getKey()) {
        case "url":
          attributes.add(new Attribute(AttributeType.URL, attribute.getValue()));
          break;
        case "title":
          attributes.add(new Attribute(AttributeType.TITLE, attribute.getValue()));
          System.out.println(attribute.getValue());
          break;
        case "description":
          attributes.add(new Attribute(AttributeType.DESCRIPTION,
              new HtmlToPlainText().getPlainText(Jsoup.parse(attribute.getValue()))));
          break;
        case "itemtype":
          item.setType(itemTypeAnnotator.annotate(item));
          break;
        case "published_date":
          attributes.add(new Attribute(AttributeType.DATE_PUBLISHED, attribute.getValue()));
          break;
        case "date":
          attributes.add(new Attribute(AttributeType.DATE, attribute.getValue()));
          break;
        case "start_date":
          attributes.add(new Attribute(AttributeType.DATE_START, attribute.getValue()));
          break;
        case "end_date":
          attributes.add(new Attribute(AttributeType.DATE_END, attribute.getValue()));
          break;
        case "venue":
          attributes.add(new Attribute(AttributeType.VENUE, attribute.getValue()));
          break;
      }
    }
    item.setAttributes(attributes);
    item = dateTagger.annotate(item);
    return item;
  }

  private static List<Item> readJson(String[] args) {
    ArrayList<Item> items = new ArrayList<Item>();

    JsonReader jsonReader;
    try {
      jsonReader = new JsonReader(new FileReader(args[0]));
      jsonReader.beginArray();
      while (jsonReader.hasNext()) {
        items.add(readItem(jsonReader));
      }
      jsonReader.endArray();
      jsonReader.close();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return items;
  }

  private static Item readItem(JsonReader jsonReader) {
    Item item = new Item();
    HashMap<String, String> jsonMap = new HashMap<String, String>();
    try {
      jsonReader.beginObject();
      while (jsonReader.hasNext()) {
        String key = jsonReader.nextName();
        String value = jsonReader.nextString();
        jsonMap.put(key, value);
      }
      jsonReader.endObject();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    item.setJson(jsonMap);
    return item;
  }
}
