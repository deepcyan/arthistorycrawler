# -*- coding: utf-8 -*-
import scrapy
from arthistevents.items import GenericItem


class Spider019Spider(scrapy.Spider):
    name = "spider019"
    allowed_domains = ["http://artefactjournal.com"]
    start_urls = (
        'http://artefactjournal.com/iaah/iaah-events/',
    )
    def parse(self, response):
        descriptions = response.xpath('//p[@style="text-align:left;"]').extract()
        titles = response.xpath('//p[@style="text-align:left;"]/strong/text()').extract()
        for title, description in zip(titles, descriptions):
            item = GenericItem()
            item['url'] = response.url
            item['title'] = title
            item['itemtype'] = 'generic'
            item['description'] = description
            yield item
