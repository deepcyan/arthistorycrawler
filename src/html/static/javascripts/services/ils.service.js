(function () {
    'use strict';

    angular
        .module('ils.services')
        .factory('ILSService', ILSService);

    ILSService.$inject = ['$http', '$cookies', 'ngThriftService', '$q'];

    /**
     * @namespace Authentication
     * @returns {Factory}
     */
    function ILSService($http, $cookies, ngThriftService, $q) {
        var session = null;
        var cart = null;
        var featuredProducts = null;
        var aHEServiceClient = ngThriftService.create(AHEServiceClient, 'http://localhost/thrift-api/v1/AHEService/');

        var ILSService = {
            get: get
        };

        function get() {
            return aHEServiceClient.getAll()
                .then(function (data) {
                        return data;
                    },
                    function (data) {
                        console.log("err: " + JSON.stringify(data));
                    }
                );
        }

        return ILSService;
    }
})();